#pragma once

#include <string>
#include <map>
class SaveData
{
public:
	static void SetInt(std::string a_name, int a_value);
	static int GetInt(std::string a_name);

	static std::map<std::string, int> savedInts;


};

