#include "Attacker.h"
#include "Game1.h"
#include "Team.h"
Attacker::Attacker(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice) : AIAgent(a_texture, a_pos, a_teamChoice)
{
	m_fieldPosition = eFIELDPOSITION::ATTACKER;
	moveSpeed = attackerSpeed;
}


Attacker::~Attacker()
{
}

void Attacker::Update(float deltaTime)
{
	Vector2 vecBetweenOppositionBall = position - m_team->GetBall()->position;
	Vector2 vecBetweenOppositionSeekPos = position - SeekPos;
	int teamMateCount = 0;

	//switch
	switch (currState)
	
	{
	case eAIState::KICK:
		AIKick(deltaTime, this, m_team->GetBall());

		//do the transitions from here
		m_currKickTimer -= deltaTime;
		if (m_currKickTimer <= 0)
		{
			currState = eAIState::SEEK;
		}

		if (vecBetweenOppositionBall.Magnitude() <= ((m_team->GetBall()->GetWidth() / 2) + (GetWidth() / 2)))
		{
			if (m_currKickTimer > 0.1f)
				m_currKickTimer = 0.1f;
		}
		//checks which way the AI is attacking (right in this case)
		if ((m_team->GetTeamDirection() == -1))
		{
			if (m_team->GetBall()->position.x > (m_windowBoundaries.x / 2))
			{
				currState = eAIState::ATTACK;
			}
		}
		else
		{
			if (m_team->GetBall()->position.x < (m_windowBoundaries.x / 2))
			{
				currState = eAIState::ATTACK;
			}
		}


		break;

	case eAIState::SEEK:
		for (int i = 0; i < m_fleePositions.Size(); i++)
		{
			Vector2 murderVec(this->position - m_fleePositions[i]->position);
			float distanceToMurder = murderVec.Magnitude();
			if (distanceToMurder <= m_avoidDistance)
			{
				AIAgent::AIAvoid(this, m_fleePositions[i]);
			}
		}
		AISeek(m_team->GetBall(), m_team->GetGoal(), this, m_windowBoundaries.x);
		
		
		

		if (vecBetweenOppositionSeekPos.Magnitude() <= ((m_team->GetBall()->GetWidth())) && currState == eAIState::SEEK)
		{
			m_currKickTimer = m_kickTime;
			currState = eAIState::KICK;
			kickDirection = vecBetweenOppositionBall;
		}
	
	break;
	case eAIState::ATTACK:
		for (int i = 0; i < m_fleePositions.Size(); i++)
		{
			Vector2 murderVec(this->position - m_fleePositions[i]->position);
			float distanceToMurder = murderVec.Magnitude();
			if (distanceToMurder <= m_avoidDistance)
			{
				AIAgent::AIAvoid(this, m_fleePositions[i]);
			}
		}
		int randResult = Utility::RandomNumberGenerator();


		// beggining of enter seek code
		for (int i = 0; i < this->GetTeam()->GetTeamMembersArray()->size(); i++)
		{
			if (this->GetTeam()->GetTeamMembers(i)->IsAlive() == true)
			{
				teamMateCount++;
			}

		}
		if (teamMateCount == 2 || teamMateCount == 1)
		{
			//AIAgent::AISeek(m_team->GetBall(), m_team->GetGoal(), this, m_windowBoundaries.x);
			currState = eAIState::SEEK;
		}
		else
		{
			currState = eAIState::ATTACK;

		}


		//checks which way the AI is attacking (right in this case)
		if ((m_team->GetTeamDirection() == 1))
		{
			if (m_team->GetBall()->position.x > (m_windowBoundaries.x / 2))
			{
				AIDeflection(m_team->GetGoal(), this, m_team->GetBall());
			}
			else
			{
				if (randResult == 0)
				{
					AIBullyDefender(this, m_team->GetOppositionTeam()->GetTeamMember(Character::eFIELDPOSITION::DEFENDER));
				}
				else if (randResult == 1)
				{
					AIFindSpace(this, m_team->GetOppositionTeam()->GetTeamMember(Character::eFIELDPOSITION::DEFENDER));
				}
			}
		}
		else
		{
			if (m_team->GetBall()->position.x < (m_windowBoundaries.x / 2))
			{
				AIDeflection(m_team->GetGoal(), this, m_team->GetBall());
			}
			else
			{
				if (randResult == 0)
				{
					AIBullyDefender(this, m_team->GetOppositionTeam()->GetTeamMember(Character::eFIELDPOSITION::DEFENDER));
				}
				else if (randResult == 1)
				{
					AIFindSpace(this, m_team->GetOppositionTeam()->GetTeamMember(Character::eFIELDPOSITION::DEFENDER));
				}
			}
		}




	}
	
	AIAgent::Update(deltaTime);
	
}
