#pragma once
#include "Character.h"
#include <vector>
class Goal;
class Ball;
class Team
{
public:
	Team();
	Team(Goal* a_teamGoal, Goal* a_oppositionGoal, Ball* a_gameBall);
	~Team();

	void AddMember(Character* a_memberToAdd);

	void SetGoal(Goal* a_goalChoice);
	Goal* GetGoal();
	void SetBall(Ball* a_ballChoice);
	Ball* GetBall();
	void SetOppositionGoal(Goal* a_goalChoice);
	Goal* GetOppositionGoal();
	void SetTeamDirection(int a_directionChoice);
	int GetTeamDirection();
	void SetOppositionTeam(Team* a_oppositionTeam);
	Team* GetOppositionTeam();
	Character* GetTeamMember(Character::eFIELDPOSITION a_pos);
	Character* GetTeamMembers(int a_teamMemberChoice);
	std::vector<Character*>* GetTeamMembersArray();
	bool getBallContact();
	void setBallContactOpposite();

private:
	int teamID;
	Goal* m_teamsGoal;
	Ball* m_gameBall;
	Goal* m_oppositionsGoal;
	int m_teamDirection;
	Team* m_oppositionTeam;
	bool m_ballContact;


	std::vector<Character*> teamMembers;
};




