#pragma once

#include "AIAgent.h"
class Midfielder : public AIAgent
{
public:
	Midfielder(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice);
	~Midfielder();

	void Update(float deltaTime);

	
};

