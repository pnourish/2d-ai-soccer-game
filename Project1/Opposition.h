#pragma once
#include "Entity.h"
#include "AIManager.h"




class Opposition : public Entity
{
public: 
	Opposition();

	Opposition(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice);

	~Opposition();


	void Update(float deltaTime);
	void SetWindowBoundaries(Vector2 a_boundaries);
	Vector2 m_windowBoundaries;

	eAIState currState = eAIState::SEEK;

};

