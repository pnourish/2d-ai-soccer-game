#pragma once
#include "Entity.h"
#include "Character.h"
#include "Texture.h"

class Input;
class SpriteBatch;
class Player : public Character
{
public:
	Player();
	Player(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice);
	~Player();

	void Update(float deltaTime, Input* a_input);
	void Sprint(Input* a_input, float deltaTime);
	
private:
	float m_moveSpeed = 50.0f;
	float sprintResetTimer = 2.0f;
	bool canUseSprint = true;
	
};

