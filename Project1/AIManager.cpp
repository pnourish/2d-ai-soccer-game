//#include "AIManager.h"
//#include "Game1.h"
//#include "AIAgent.h"
//#include "Ball.h"
//#include "Goal.h"
//
//AIManager::AIManager()
//{
//}
//
//
//AIManager::~AIManager()
//{
//}
//
//
//
//void AIManager::AISeek(Ball* a_ball, Goal* a_oppositionGoal, AIAgent* a_opposition, float a_windowWidth)
//{
//	////AI movement towards the ball
//	//avoidSpeed = -20;
//
//	// the following line works out a vector that points from the goals location to the balls current location
//	Game1::vecFromGoal = (a_ball->position - a_oppositionGoal->position);
//
//	// the seekPos is where we want the ball to aim to get. it determines this by adding the vecFromGoal vector to the balls position plus a certain offset
//	Game1::SeekPos = a_ball->position + Game1::vecFromGoal.Normalised() * m_seekPosOffset;
//
//	// this is a vector to the seek position from the current location of the opposition
//	Vector2 vecBetweenSeekOpposition = Game1::SeekPos - a_opposition->position;
//
//	//this is a vector going from the ball to the opposition
//	Vector2 vecBetweenOppositionBall = a_opposition->position - a_ball->position;
//
//
//	//determine the distance between the opposition and the goal they are attacking
//	//Vector2 vecBetweenOppositionAndGoal = a_opposition->position - a_oppositionGoal->position;
//	//float distanceBetweenOppositionAndGoal = vecBetweenOppositionAndGoal.Magnitude();asd
//
//	
//
//	//next two lines check for collision between opposition and seekPos
//	Vector2 vecBetweenOppositionSeekPos = a_opposition->position - Game1::SeekPos;
//	//this function checks to see if the seekpos has been reached so that we can change to the seek condition
//	//if (vecBetweenOppositionSeekPos.Magnitude() <= ((a_ball->GetWidth())) && a_opposition-> currState == eAIState::SEEK /*&& distanceBetweenOppositionAndGoal <= (a_windowWidth/2 )*/)
//	//{
//	//	m_currKickTimer = m_kickTime;
//	//	a_opposition->currState = eAIState::KICK;
//	//	kickDirection = vecBetweenOppositionBall;
//	//}
//
//	if (vecBetweenSeekOpposition.Magnitude() > 40.0f)
//	{
//		// this sets the oppositions velcotiy to their current velocity plus a normalised vector pointing at the ball
//		// the multiplication of a negative number makes it that the opposition is now actually trying to avoid the ball.
//		// this avoidance makes it that the opposition will curve around the ball to get to the seekPos.
//		a_opposition->SetVelocity(a_opposition->GetVelocity() + (a_ball->position - a_opposition->position).Normalised() * -avoidSpeed);
//		// this sets the oppositions velcotiy to their current velocity plus a vector pointing at the seekPos. this is how we cause movement to the seekPos and if fast enough how we eventually hit the ball
//		a_opposition->SetVelocity(a_opposition->GetVelocity() + (Game1::SeekPos - a_opposition->position).Normalised() * moveSpeed);
//	}
//	else
//	{
//		a_opposition->SetVelocity(a_opposition->GetVelocity() + vecBetweenOppositionBall.Normalised()*-40.0f);
//	}
//
//	//if (a_ball->position.x > (a_windowWidth / 2))
//	//{
//	//	a_opposition->currState = eAIState::DEFEND;
//	//}
//}
//
//
//void AIManager::AIDefend(Ball* a_ball, Goal*a_playerGoal, AIAgent* a_opposition, float a_windowWidth)
//{
//	////AI movement towards the ball
//	avoidSpeed = -20;
//
//	// the following line works out a vector that points from the goals location to the balls current location
//	Game1::vecFromGoal = (a_ball->position - a_playerGoal->position);
//
//	// the seekPos is where we want the ball to aim to get. it determines this by adding the vecFromGoal vector to the balls position plus a certain offset
//	Game1::SeekPos = a_ball->position + Game1::vecFromGoal.Normalised() * -m_seekPosOffset;
//
//	// this is a vector to the seek position from the current location of the opposition
//	Vector2 vecBetweenSeekOpposition = Game1::SeekPos - a_opposition->position;
//
//	//this is a vector going from the ball to the opposition
//	Vector2 vecBetweenOppositionBall = a_opposition->position - a_ball->position;
//
//
//	if (vecBetweenSeekOpposition.Magnitude() > 40.0f)
//	{
//		// this sets the oppositions velcotiy to their current velocity plus a normalised vector pointing at the ball
//		// the multiplication of a negative number makes it that the opposition is now actually trying to avoid the ball.
//		// this avoidance makes it that the opposition will curve around the ball to get to the seekPos.
//		a_opposition->SetVelocity(a_opposition->GetVelocity() + (a_ball->position - a_opposition->position).Normalised() * -avoidSpeed);
//		// this sets the oppositions velcotiy to their current velocity plus a vector pointing at the seekPos. this is how we cause movement to the seekPos and if fast enough how we eventually hit the ball
//		a_opposition->SetVelocity(a_opposition->GetVelocity() + (Game1::SeekPos - a_opposition->position).Normalised() * moveSpeed);
//	}
//
//	//if (a_ball->position.x < (a_windowWidth / 2))
//	//{
//	//	a_opposition->currState = eAIState::SEEK;
//	//}
//}
//
