#pragma once

#include "Entity.h"
#include "Character.h"
#include "Texture.h"
#include "Ball.h"



enum class e_playerState
{
	WAITING,
	TOUCHED
};



class Input;
class SpriteBatch;
class MarkovPlayer : public Character
{
public: 
	MarkovPlayer() = delete;
	MarkovPlayer(Ball & a_gameBall, Texture* a_texture, Vector2 a_pos, Team* a_teamChoice);
	~MarkovPlayer();

	void Update(float deltaTime, Input* a_input);

	Ball & m_gameBall;

	e_playerState currState = e_playerState::WAITING;

	int m_playerID;

	float m_moveSpeed = 400.0f;

	bool m_touchedBall;
	float touchedFullTimer = 2.0f;;
	float touchedTimer = touchedFullTimer;


private:


};
