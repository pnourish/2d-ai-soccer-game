#pragma once
#include <iostream>




class Vector2
{
public:
	Vector2();
	Vector2(float a_x, float a_y);
	~Vector2();


	bool operator == (Vector2 & other)
	{
		return x == other.x && y == other.y;
	}

	Vector2 operator + (Vector2  &  other)
	{
		Vector2 returnValue;
		returnValue.x = x + other.x;
		returnValue.y = y + other.y;
		return returnValue;
	}
	
	Vector2 operator - (Vector2  &  other)
	{
		Vector2 returnValue;
		returnValue.x = x - other.x;
		returnValue.y = y - other.y;
		return returnValue;
	}

	Vector2 operator += (Vector2 & other)
	{
		Vector2 returnValue;
		returnValue.x = x + other.x;
		returnValue.y = y + other.y;
		return returnValue;
	}
	
	Vector2 operator -= (Vector2 & other)
	{
		Vector2 returnValue;
		returnValue.x = x - other.x;
		returnValue.y = y - other.y;
		return returnValue;
	}

	Vector2 operator * (float other)
	{
		Vector2 returnValue;
		returnValue.x = x * other;
		returnValue.y = y * other;
		return returnValue;
	}


	Vector2 operator / (float other)
	{
		Vector2 returnValue;
		returnValue.x = x / other;
		returnValue.y = y / other;
		return returnValue;
	}

	friend std::ostream &operator << (std::ostream &output, const Vector2 &vec1)
	{
		output << "vec2(" << vec1.x << " , " << vec1.y << ")";
		return output;
	}


	float clamp(float number, float min, float max);


	float Magnitude();
	Vector2 Normalised();
	void Normalise();
	float dot(const Vector2& Other)const;
	float angle(const Vector2& other)const;
	float signedAngle(const Vector2& other)const;
	Vector2 rightPerp()const;
	Vector2 leftPerp()const;


	float x;
	float y;
};

