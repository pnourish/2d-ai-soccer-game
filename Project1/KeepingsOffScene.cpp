#include "KeepingsOffScene.h"
#include "SpriteBatch.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "glfw3.h"
#include "Application.h"
#include "Vector2.h"
#include <iostream>
#include <amp.h>
#include "amp.h"
#include <ctime>
#include "AIAgent.h"
#include "Midfielder.h"
#include "DynamicArrayTemplate.h"
#include "team.h"

#include "SoccerDebugDraw.h"
#include "SaveData.h"


int KeepingsOffScene::PlayerScore;
int KeepingsOffScene::oppositionScore;
float KeepingsOffScene::disableTimer = 4;
float KeepingsOffScene::disableLength = 4;
Vector2 KeepingsOffScene::vecFromGoal;
Vector2 KeepingsOffScene::SeekPos;

KeepingsOffScene::KeepingsOffScene()
{
}


KeepingsOffScene::~KeepingsOffScene()
{
}

void KeepingsOffScene::Startup()
{
	
	m_input = Input::GetSingleton();
	// Load in the textures that we want to use
	m_backgroundTexture = new Texture("./Resources/Images/background.png");
	m_pAgentTexture = new Texture("./Resources/Images/Agent.png");
	m_soccerBallTextture = new Texture("./Resources/Images/soccerballblackbackground1.png");
	m_playerTexture = new Texture("./Resources/Images/Player Blue Colour.png");
	m_playerTeamTexture = new Texture("./Resources/Images/Blue Colour.png");
	m_oppositionTexture = new Texture("./Resources/Images/Red Colour.png");
	
	//Vector2 playerPos;
	Vector2 ballPos;
	Vector2 oppositionPos[3];
	Vector2 teamMatePos[3];
//	Vector2 murderBotPos[2];
	//playerPos.x = windowWidth *0.1f;
	//playerPos.y = ((windowHeight / 2)* 1);
	ballPos.x = (windowWidth / 2);
	ballPos.y = (windowHeight / 2);
	oppositionPos[0].x = windowWidth  * 0.9f;
	oppositionPos[0].y = ((windowHeight / 2)*0.5);
	oppositionPos[1].x = windowWidth  * 0.9f;
	oppositionPos[1].y = ((windowHeight / 2));
	oppositionPos[2].x = windowWidth  * 0.9f;
	oppositionPos[2].y = ((windowHeight / 2)* 1.5);
	teamMatePos[0].x = windowWidth  * 0.1f;
	teamMatePos[0].y = ((windowHeight / 2));
	teamMatePos[1].x = windowWidth  * 0.1f;
	teamMatePos[1].y = ((windowHeight / 2)*0.5);
	teamMatePos[2].x = windowWidth  * 0.1f;
	teamMatePos[2].y = ((windowHeight / 2)* 1.5);

	//creates player, ball and opposition and specifies their textures and starting positions
	m_player = new Player(m_playerTexture, teamMatePos[0], &team1);
	m_ball = new Ball(m_soccerBallTextture, ballPos);






	team1.SetBall(m_ball);
	team2.SetBall(m_ball);



	team1.SetTeamDirection(1);
	team2.SetTeamDirection(-1);

	team1.SetOppositionTeam(&team2);
	team2.SetOppositionTeam(&team1);


	oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[0], &team2));
	oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[1], &team2));
	oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));
	//oppositionArray.Add(new Midfielder(m_oppositionTexture, oppositionPos[2], &team2));

	for (int i = 0; i < numberofenemies; i++)
	{
		oppositionArray[i]->SetSpawn(oppositionPos[i]);
	}
	team2.AddMember(oppositionArray[0]);
	team2.AddMember(oppositionArray[1]);
	team2.AddMember(oppositionArray[2]);

	teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));
	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[1], &team1));

	//teamMateArray.Add(new Midfielder(m_playerTeamTexture, teamMatePos[2], &team1));

	for (int i = 0; i < numberofteammates; i++)
	{
		teamMateArray[i]->SetSpawn(teamMatePos[i + 1]);
	}

	team1.AddMember(teamMateArray[0]);
	team1.AddMember(m_player);
	team1.AddMember(teamMateArray[1]);




	m_player->SetSpawn(teamMatePos[0]);
	m_ball->SetSpawn(ballPos);

	//ScoreRestart();
	// Create the font for use with draw string
	m_font = new Font("./Resources/Fonts/calibri_36px.fnt");
	//informing all entities of the boundaries of the game
	m_player->SetWindowBoundaries(Vector2(windowWidth, windowHeight));
	m_ball->SetWindowBoundaries(Vector2(windowWidth, windowHeight));
	for (int i = 0; i < oppositionArray.Size(); i++)
	{
		oppositionArray[i]->SetWindowBoundaries(Vector2(windowWidth, windowHeight));
	}
	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		teamMateArray[i]->SetWindowBoundaries(Vector2(windowWidth, windowHeight));
	}

	m_mouseX = 0;
	m_mouseY = 0;

	
}



void KeepingsOffScene::Update(float deltaTime)
{
	SoccerDebugDraw::GetInstance()->Clear();

	static bool deltaTimeChecked = false;
	if (deltaTimeChecked == false)
	{
		deltaTime = 0.0f;
		deltaTimeChecked = true;
	}
	m_DeltaTime = deltaTime;
	bool gameIsRunning = true;
	m_input->GetMouseXY(&m_mouseX, &m_mouseY);
	disableTimer -= deltaTime;
	if (disableTimer <= 1)
	{
		DeltaTimeUpdate(deltaTime);
	}


}


void KeepingsOffScene::DeltaTimeUpdate(float deltaTime)
{
	m_gameTimer -= deltaTime;
	m_ball->Update(deltaTime);
	if (m_ball->position.x != 0 || m_ball->position.y != 0)
	{
		m_ball->Drag();
	}
	//various collision function calls
	//opposition and ball callision
	for (int i = 0; i < oppositionArray.Size(); i++)
	{
		if (oppositionArray[i]->IsAlive() == true)
			if (collisionManager.AIAndBallCollision(oppositionArray[i], m_ball) == true)
			{
				oppositionScore++;
			}
	}
	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (teamMateArray[i]->IsAlive() == true)
			if (collisionManager.AIAndBallCollision(teamMateArray[i], m_ball) == true)
			{
				PlayerScore++;
			}
	}
	//player and ball collision
	if (m_player->IsAlive() == true)
		if (collisionManager.PlayerAndBallCollision(m_player, m_ball) == true)
		{
			PlayerScore++;
		}

	//player and opposition collision
	for (int i = 0; i < oppositionArray.Size(); i++)
	{
		if (m_player->IsAlive() == true && oppositionArray[i]->IsAlive() == true)

			collisionManager.PlayerAndAiCollision(m_player, oppositionArray[i]);
	}
	//player and team mate collision
	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (m_player->IsAlive() == true && teamMateArray[i]->IsAlive() == true)
			collisionManager.PlayerAndAiCollision(m_player, teamMateArray[i]);
	}

	int mapSize = 1000;


	//ai and ai collision
	// opposition colliding with other opposition
	for (int i = 0; i < oppositionArray.Size(); i++)
	{
		if (oppositionArray[i]->IsAlive() == true)

			for (int j = 0; j < oppositionArray.Size(); j++)
			{
				if (oppositionArray[j]->IsAlive() == true)
				{
					if (i == j)
						continue;
					collisionManager.AiAndAiCollision(oppositionArray[i], oppositionArray[j]);
				}
			}
	}
	//team mates colliding with other team mates
	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (teamMateArray[i]->IsAlive() == true)
			for (int j = 0; j < teamMateArray.Size(); j++)
			{
				if (teamMateArray[j]->IsAlive() == true)
				{
					if (i == j)
						continue;
					collisionManager.AiAndAiCollision(teamMateArray[i], teamMateArray[j]);
				}
			}
	}
	//team mates colliding with opposition
	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (teamMateArray[i]->IsAlive() == true)
		{
			for (int j = 0; j < oppositionArray.Size(); j++)
			{
				if (oppositionArray[j]->IsAlive() == true)
					collisionManager.AiAndAiCollision(teamMateArray[i], oppositionArray[j]);
			}
		}
	}

	//opposition colliding with team mates
	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (teamMateArray[i]->IsAlive() == true)
		{
			for (int j = 0; j < oppositionArray.Size(); j++)
			{
				if (oppositionArray[j]->IsAlive() == true)
					collisionManager.AiAndAiCollision(oppositionArray[j], teamMateArray[i]);
			}
		}
	}


	//calls update function on all opposition AIs
	for (int i = 0; i < oppositionArray.Size(); i++)
	{
		if (oppositionArray[i]->IsAlive() == true) 
			oppositionArray[i]->Update(deltaTime);
	}

	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (teamMateArray[i]->IsAlive() == true) 
			teamMateArray[i]->Update(deltaTime);
	}


	//restores game to starting settings if game timer runs out
	if (m_gameTimer <= 1)
	{
	
		/*	for (int i = 0; i < oppositionArray.Size(); i++)
		{
			oppositionArray[i]->Respawn();
		}

		for (int i = 0; i < teamMateArray.Size(); i++)
		{
			teamMateArray[i]->Respawn();
		}
		m_player->Respawn();
		m_ball->Respawn();
		ResetScores();
		KeepingsOffScene::disableTimer = KeepingsOffScene::disableLength; */
		hasGameEnded = true;
		
	}
	m_player->Update(deltaTime, m_input);
}

void KeepingsOffScene::Draw(SpriteBatch* m_spritebatch)
{

	// clear the back buffer


	m_spritebatch->SetRenderColor(80, 80, 80, 255);
	m_spritebatch->DrawLine((windowWidth / 2), 0 - 100, (windowWidth / 2), windowHeight + 100, 5);
	m_spritebatch->SetRenderColor(255, 255, 255, 255);
	//draw entity
	if (m_player->IsAlive() == true)
		m_player->Draw(m_spritebatch);
	m_ball->Draw(m_spritebatch);
	for (int i = 0; i < oppositionArray.Size(); i++)
	{
		if (oppositionArray[i]->IsAlive() == true)
			oppositionArray[i]->Draw(m_spritebatch);
	}

	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (teamMateArray[i]->IsAlive() == true)
			teamMateArray[i]->Draw(m_spritebatch);
	}

	//this line resets the render colour to white
	m_spritebatch->SetRenderColor(255, 255, 255, 255);

	//these three lines draw the deltatime in the top left corner
	char buffer[100];
	itoa(m_DeltaTime * 1000.0f, buffer, 10);
	m_spritebatch->DrawString(m_font, buffer, 10, 10);

	// these lines set a location for where i want the player score to be displayed and then they display it
	//the setrenderColor function lets me change the colour that is being rendered to whatever RGB value i want
	// have to remember to reset it afterwards and the fourth number is opacity
	float playerScoreLoc = 0;
	playerScoreLoc = (windowWidth * 0.4);
	m_spritebatch->SetRenderColor(33, 15, 255, 255);
	char buffer2[10];
	itoa(PlayerScore, buffer2, 10);
	m_spritebatch->DrawString(m_font, buffer2, playerScoreLoc, 10);

	// these lines set a location for where i want the opposition score to be displayed and then they display it
	//the setrenderColor function lets me change the colour that is being rendered to whatever RGB value i want
	// have to remember to reset it afterwards and the fourth number is opacity
	float oppositionScoreLoc = 0;
	oppositionScoreLoc = (windowWidth * 0.6);
	m_spritebatch->SetRenderColor(237, 28, 36, 255);
	char buffer3[10];
	itoa(oppositionScore, buffer3, 10);
	m_spritebatch->DrawString(m_font, buffer3, oppositionScoreLoc, 10);
	//this line resets the render colour to white
	m_spritebatch->SetRenderColor(255, 255, 255, 255);

	float gameTimerLoc = 0.0f;
	gameTimerLoc = (windowWidth * 0.48);
	m_spritebatch->SetRenderColor(255, 255, 255, 255);
	char buffer4[10];
	itoa(m_gameTimer, buffer4, 10);
	m_spritebatch->DrawString(m_font, buffer4, gameTimerLoc, 10);

	if (disableTimer >= 1.0f)
	{
		float disableTimerLoc = 0.0f;
		disableTimerLoc = (windowWidth * 0.48);
		m_spritebatch->SetRenderColor(255, 255, 255, 255);
		char buffer5[10];
		itoa(disableTimer, buffer5, 10);
		m_spritebatch->DrawString(m_font, buffer5, gameTimerLoc, 50);
	}
	//	DrawPoint(SeekPos, SpriteBatch* m_spritebatch);
	//	DrawVector(m_oppositionGoal->position, vecFromGoal, SpriteBatch* m_spritebatch);
	//	DrawVector(m_opposition->position, m_opposition->GetVelocity(), SpriteBatch* m_spritebatch);


	SoccerDebugDraw::GetInstance()->Draw(m_spritebatch);

}

void KeepingsOffScene::Shutdown()
{

	SaveData::SetInt("PlayerScore", PlayerScore);
	SaveData::SetInt("OppositionScore", oppositionScore);
	m_gameTimer = m_roundLength;
	PlayerScore = 0;
	oppositionScore = 0;
	delete m_pAgentTexture;
	delete m_backgroundTexture;
	delete m_player;
	delete m_font;
	delete m_soccerBallTextture;
	delete m_playerTexture;
	delete m_playerTeamTexture;

	for (int i = 0; i < numberofenemies; i++)
	{
		delete oppositionArray[i];
	}

	oppositionArray.Clear();
	for (int i = 0; i < numberofteammates; i++)
	{
		delete teamMateArray[i];
	}

	teamMateArray.Clear();

	delete m_oppositionTexture;
	delete m_ball;

}


void KeepingsOffScene::DrawVector(Vector2 origin, Vector2 dir)
{
	m_spritebatch->DrawLine(origin.x, origin.y, origin.x + dir.x, origin.y + dir.y);
}
void KeepingsOffScene::DrawPoint(Vector2 origin)
{
	m_spritebatch->DrawLine(origin.x - 5.0f, origin.y - 5.0f, origin.x + 5.0f, origin.y + 5.0f);
	m_spritebatch->DrawLine(origin.x + 5.0f, origin.y - 5.0f, origin.x - 5.0f, origin.y + 5.0f);
}
void KeepingsOffScene::ResetScores()
{
	PlayerScore = 0;
	oppositionScore = 0;
}
void KeepingsOffScene::AddScore(int iD)
{
	if (iD == 0)
	{
		PlayerScore += 1;
	}
	else if (iD == 1)
	{
		oppositionScore += 1;
	}
	KeepingsOffScene::disableTimer = KeepingsOffScene::disableLength;
}