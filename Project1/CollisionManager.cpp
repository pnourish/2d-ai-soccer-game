#include "CollisionManager.h"
#include "Game1.h"



CollisionManager::CollisionManager()
{
}
CollisionManager::~CollisionManager()
{
}
bool CollisionManager::PlayerAndBallCollision(Player* a_player, Ball* a_ball)
{
	Vector2 vecBetweenPlayerBall = a_player->position - a_ball->position;
	if (vecBetweenPlayerBall.Magnitude() <= ((a_ball->GetWidth() / 2) + (a_player->GetWidth() / 2)))
	{
		//TODO - fix this shit.
		a_ball->SetVelocity(((vecBetweenPlayerBall.Normalised() * -1)  + a_player->GetVelocity()*0.001f) * 500.0f);
		//std::cout << "Player Touch!" << std::endl;
		return true;
	}
}
bool CollisionManager::AIAndBallCollision(AIAgent* a_opposition, Ball* a_ball)
{
	Vector2 vecBetweenOppositionBall = a_opposition->position - a_ball->position;


	if (vecBetweenOppositionBall.Magnitude() <= ((a_ball->GetWidth() / 2) + (a_opposition->GetWidth() / 2)))
	{
		
		a_ball->SetVelocity(vecBetweenOppositionBall * -15.0f);
		//std::cout << "Opposition Touch!" << std::endl;
		return true;
	}
}
bool CollisionManager::BallAndPlayerGoalCollision(Ball* a_ball, Goal* a_playerGoal, AIAgent* a_opposition, Player* a_player)
{
	Vector2 closestPointPlayerGoal;
	Vector2 minPlayergoal(a_playerGoal->position.x - a_playerGoal->GetWidth() / 2, a_playerGoal->position.y - a_playerGoal->GetHeight() / 2);
	Vector2 maxPlayergoal(a_playerGoal->position.x + a_playerGoal->GetWidth() / 2, a_playerGoal->position.y + a_playerGoal->GetHeight() / 2);
	closestPointPlayerGoal.x = closestPointPlayerGoal.clamp(a_ball->position.x, minPlayergoal.x, maxPlayergoal.x);
	closestPointPlayerGoal.y = closestPointPlayerGoal.clamp(a_ball->position.y, minPlayergoal.y, maxPlayergoal.y);
	Vector2 distanceToPlayerGoal = a_ball->position - closestPointPlayerGoal;
	if (distanceToPlayerGoal.Magnitude() <= ((a_ball->GetWidth() / 2)))
	{
		return true;
	}
	return false;
}
bool CollisionManager::BallAndOppositionGoalCollision(Ball* a_ball, Goal* a_playerGoal, AIAgent* a_opposition, Player* a_player)
{
	Vector2 closestPointOppositionGoal;
	Vector2 minoppositiongoal(a_playerGoal->position.x - a_playerGoal->GetWidth() / 2, a_playerGoal->position.y - a_playerGoal->GetHeight() / 2);
	Vector2 maxoppositiongoal(a_playerGoal->position.x + a_playerGoal->GetWidth() / 2, a_playerGoal->position.y + a_playerGoal->GetHeight() / 2);
	closestPointOppositionGoal.x = closestPointOppositionGoal.clamp(a_ball->position.x, minoppositiongoal.x, maxoppositiongoal.x);
	closestPointOppositionGoal.y = closestPointOppositionGoal.clamp(a_ball->position.y, minoppositiongoal.y, maxoppositiongoal.y);
	Vector2 distanceToOppositionGoal = a_ball->position - closestPointOppositionGoal;
	if (distanceToOppositionGoal.Magnitude() <= ((a_ball->GetWidth() / 2)))
	{
		return true;
	}
	return false;
}
bool CollisionManager::PlayerAndAiCollision(Player* a_player, AIAgent* a_Ai)
{
	Vector2 vecBetweenPlayerAi = a_player->position - a_Ai->position;
	if (vecBetweenPlayerAi.Magnitude() <= ((a_player->GetWidth() / 2) + (a_Ai->GetWidth() / 2)))
	{
		a_Ai->SetVelocity(a_Ai->GetVelocity() + vecBetweenPlayerAi * -20.0f);

		//set position
		a_player->position = a_Ai->position + vecBetweenPlayerAi.Normalised() * ((a_player->GetWidth() / 2) + (a_Ai->GetWidth() / 2));

		return true;
	}
}
bool CollisionManager::AiAndAiCollision(AIAgent* a_firstAi, AIAgent* a_secondAi)
{
	Vector2 vecBetweenAiAndAi = a_firstAi->position - a_secondAi->position;
	if (vecBetweenAiAndAi.Magnitude() <= ((a_secondAi->GetWidth() / 2) + (a_firstAi->GetWidth() / 2)))
	{
		//TODO - fix this shit.
		a_secondAi->SetVelocity((((vecBetweenAiAndAi.Normalised() * -1) + a_firstAi->GetVelocity()*0.001f) * 500.0f));
		a_firstAi->SetVelocity((((vecBetweenAiAndAi.Normalised() * 1) + a_firstAi->GetVelocity()*0.001f) * 500.0f));
		a_firstAi->position = a_secondAi->position + vecBetweenAiAndAi.Normalised() * ((a_firstAi->GetWidth() / 2) + (a_secondAi->GetWidth() / 2));

		return true;
	}
}

bool CollisionManager::AiAndMurderBotCollision(AIAgent * a_AI, MurderBot * a_murderBot)
{
	Vector2 vecBetweenAIandMurder = a_AI->position - a_murderBot->position;

//	std::cout << "ATTEMPTED MURDER HAS OCCURED" << std::endl;
	if (vecBetweenAIandMurder.Magnitude() <= ((a_murderBot->GetWidth() / 2) + (a_AI->GetWidth() / 2)))
	{
		a_AI->position = a_murderBot->position + vecBetweenAIandMurder.Normalised() * ((a_AI->GetWidth() / 2) + (a_murderBot->GetWidth() / 2));
		a_AI->SetDead();
		return true;
	}
}

bool CollisionManager::playerAndMurderBotCollision(Player * a_player, MurderBot * a_murderBot)
{
	Vector2 vecBetweenPlayerandMurder = a_player->position - a_murderBot->position;
	if (vecBetweenPlayerandMurder.Magnitude() <= ((a_murderBot->GetWidth() / 2) + (a_player->GetWidth() / 2)))
	{
		std::cout << "A MURDER HAS OCCURED" << std::endl;
		a_player->SetDead();
		return true;
	}
}

bool CollisionManager::BallAndMurderBotCollision(Ball * a_ball, MurderBot * a_murderBot)
{
	Vector2 vecBetweenMurderBotBall = a_murderBot->position - a_ball->position;


	if (vecBetweenMurderBotBall.Magnitude() <= ((a_ball->GetWidth() / 2) + (a_murderBot->GetWidth() / 2)))
	{


		a_ball->SetVelocity(vecBetweenMurderBotBall * -15.0f);
		//std::cout << "MurderBot Touch!" << std::endl;
		return true;
	}
}

bool CollisionManager::MarkovPlayerAndBallCollision(MarkovPlayer * a_markovPlayer, Ball * a_ball)
{
	Vector2 vecBetweenPlayerBall = a_markovPlayer->position - a_ball->position;

	if (vecBetweenPlayerBall.Magnitude() <= ((a_ball->GetWidth() / 2) + (a_markovPlayer->GetWidth() / 2)))
	{
		a_ball->recentlyTouched = true;
	}
	return true;
}

