#include "MarkovPlayer.h"




MarkovPlayer::MarkovPlayer(Ball & a_gameBall, Texture* a_texture, Vector2 a_pos, Team* a_teamChoice)
	: m_gameBall(a_gameBall)
{
	m_entityTexture = a_texture;
	position.x = a_pos.x;
	position.y = a_pos.y;
	SetTeam(a_teamChoice);

}


MarkovPlayer::~MarkovPlayer()
{
}


void MarkovPlayer::Update(float deltaTime, Input* a_input)
{
	switch (currState)
	{
		//Vector2 vectorToBall = position - m_gameBall.position;

		case e_playerState::WAITING:
		{
			Vector2 vectorToBall = position - m_gameBall.position;
			if (vectorToBall.Magnitude() <=  + (this->GetWidth() / 2))
			{
				m_gameBall.SetVelocity(Vector2(0, 0));
				currState = e_playerState::TOUCHED;
			}


			break;
		}
		case e_playerState::TOUCHED:
		{
			Vector2 vectorToBall = position - m_gameBall.position;
			m_touchedBall = true;
			if (a_input->IsKeyDown(GLFW_KEY_D) || a_input->IsKeyDown(GLFW_KEY_RIGHT))
			{
				m_gameBall.SetVelocity(Vector2(m_moveSpeed, m_gameBall.GetVelocity().y));
			}
			if (a_input->IsKeyDown(GLFW_KEY_A) || a_input->IsKeyDown(GLFW_KEY_LEFT))
			{
				m_gameBall.SetVelocity(Vector2(-m_moveSpeed, m_gameBall.GetVelocity().y));
			}
			if (a_input->IsKeyDown(GLFW_KEY_W) || a_input->IsKeyDown(GLFW_KEY_UP))
			{
				m_gameBall.SetVelocity(Vector2(m_gameBall.GetVelocity().x, -m_moveSpeed));
			}
			if (a_input->IsKeyDown(GLFW_KEY_S) || a_input->IsKeyDown(GLFW_KEY_DOWN))
			{
				m_gameBall.SetVelocity(Vector2(m_gameBall.GetVelocity().x, m_moveSpeed));
			}
			if (m_touchedBall == true)
			{
			//touchedTimer -= deltaTime;
			//if (touchedTimer <= 0)
			//{
				if (vectorToBall.Magnitude() >= ((m_gameBall.GetWidth() / 2) + (this->GetWidth() / 2)))
				{
					touchedTimer = false;
					touchedTimer = touchedFullTimer;
					currState = e_playerState::WAITING;
					m_gameBall.OnKick(deltaTime, m_playerID);
				}
			//}
			}

			break;

		}
	}
}