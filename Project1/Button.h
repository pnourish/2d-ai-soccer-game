#pragma once
#include "Font.h"
#include "SpriteBatch.h"
#include "Vector2.h"
class Button
{
public:
	Button();
	Button(Vector2 a_position, float a_height, float a_width, Font* a_textFont);
	~Button();

	//this will update the object
	void Update(float deltaTime);
	
	//this will draw the object each frame
	void Draw(SpriteBatch* m_spritebatch);
	
	//This function will check if an object(possibly mouse) is within the confines
	bool checkPos(Vector2 a_objectToCheck);
	
	void setMessage(std::string messageToAdd);
	
	
	// This will be where the starting corner of the box is
	Vector2 m_position;
	float m_height;
	float m_width;
	Font* m_textFont;
	std::string m_message;
	


			
};

