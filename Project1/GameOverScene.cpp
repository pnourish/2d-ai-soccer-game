#include "GameOverScene.h"

#include "SaveData.h"

GameOverScene::GameOverScene()
{

}


GameOverScene::~GameOverScene()
{
}

void GameOverScene::Startup()
{
	playerScore = SaveData::GetInt("PlayerScore");
	opponentScore = SaveData::GetInt("OppositionScore");

	m_font = new Font("./Resources/Fonts/calibri_36px.fnt");


	float wordSize = 16 * 14;




	std::string OpponentScoreButtonText = "              " + std::to_string( opponentScore);
	OpponentScoreButton = new Button(Vector2((windowWidth * 0.75) - (wordSize / 2), windowHeight * 0.2), 45, wordSize, m_font);
	OpponentScoreButton->setMessage(OpponentScoreButtonText.c_str());
	  
	std::string PlayerScoreButtonText = "             " + std::to_string(playerScore);
	PlayerScoreButton = new Button(Vector2((windowWidth * 0.25) - (wordSize / 2), windowHeight * 0.2), 45, wordSize, m_font);
	PlayerScoreButton->setMessage(PlayerScoreButtonText.c_str());


	std::string MainMenuText = "      Main Menu";
	MainMenu = new Button(Vector2((windowWidth * 0.5) - (wordSize / 2), windowHeight * 0.3), 45, wordSize, m_font);
	MainMenu->setMessage(MainMenuText.c_str());

	std::string ReMatchText = "       Rematch";
	ReMatch = new Button(Vector2((windowWidth * 0.5) - (wordSize / 2), windowHeight * 0.4), 45, wordSize, m_font);
	ReMatch->setMessage(ReMatchText.c_str());

	std::string QuitText = "            Quit";
	Quit = new Button(Vector2((windowWidth * 0.5) - (wordSize / 2), windowHeight * 0.5), 45, wordSize, m_font);
	Quit->setMessage(QuitText.c_str());
	
	
	if (opponentScore < playerScore)
	{
		WhoWonText = "         You Won!";
	}
	else if (opponentScore > playerScore)
	{
		WhoWonText = "        You Lost!";
	}
	else
	{
		WhoWonText = "           Draw!";

	}
	WhoWon = new Button(Vector2((windowWidth * 0.5) - (wordSize / 2), windowHeight * 0.2), 45, wordSize, m_font);
	WhoWon->setMessage(WhoWonText.c_str());
}

void GameOverScene::Update()
{
}

void GameOverScene::Draw(SpriteBatch* m_spritebatch)
{
	
	//sets render colour to player colour
	m_spritebatch->SetRenderColor(33, 15, 255, 255);
	PlayerScoreButton->Draw(m_spritebatch);

	//sets render colour to opposition colour
	m_spritebatch->SetRenderColor(237, 28, 36, 255);
	OpponentScoreButton->Draw(m_spritebatch);

	//return render colour to white
	m_spritebatch->SetRenderColor(255, 255, 255, 255);
	WhoWon->Draw(m_spritebatch);
	MainMenu->Draw(m_spritebatch);
	ReMatch->Draw(m_spritebatch);
	Quit->Draw(m_spritebatch);

}

void GameOverScene::Shutdown()
{
	delete OpponentScoreButton;
	delete PlayerScoreButton;
	delete WhoWon;
	delete MainMenu;
	delete ReMatch;
	delete Quit;
	delete m_font;


}
