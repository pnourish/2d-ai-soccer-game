#include "Game1.h"
#include <crtdbg.h>
#include <ctime>


#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifdef _DEBUG
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif
int main(int argc, char **argv)
{

	srand(time(NULL));

	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );

	Game1 *pGame = new Game1(1232, 878, false, "Game1");
	pGame->RunGame();
	delete pGame;

	return 0;
};