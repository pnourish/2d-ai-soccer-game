#pragma once

#include "AIAgent.h"
class Defender : public AIAgent
{
public:
	Defender(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice);
	~Defender();

	void Update(float deltaTime);

};

