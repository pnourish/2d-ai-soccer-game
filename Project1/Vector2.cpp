#include "Vector2.h"



Vector2::Vector2()
{
	x = 0;
	y = 0;
}

Vector2::Vector2(float a_x, float a_y)
{
	x = a_x;
	y = a_y;
}

Vector2::~Vector2()
{
}

float Vector2::Magnitude()
{
	float magnitude = sqrt((x*x) + (y*y));
	return magnitude;
}



Vector2 Vector2::Normalised()
{
	Vector2 normVec;
	float mag = Magnitude();
	if (mag == 0.0f)
	{
		return normVec;
	}
	normVec.x = x / mag;
	normVec.y = y / mag;
	return normVec;
}

void Vector2::Normalise()
{
	float mag = Magnitude();
	if (mag == 0.0f)
	{
		return;
	}
	x = x / mag;
	y = y / mag;
}

float Vector2::clamp(float number, float min, float max)
{
	if (number < min)
		return min;
	else if (number > max)
		return max;
	else
		return number;
}

float Vector2::dot(const Vector2& other)const
{

	float dotValue = (this->x * other.x) + (this->y * other.y);
	return dotValue;

}

float Vector2::angle(const Vector2& other)const
{
	float dotValue = this->dot(other);
	float angle = acosf(dotValue);
	return angle;

}

Vector2 Vector2::rightPerp()const
{
	return Vector2(y, -x);
}

Vector2 Vector2::leftPerp()const
{
	return Vector2(-y, x);
}

float Vector2::signedAngle(const Vector2& other)const
{
	float angle = this->angle(other);

	float sign = this->rightPerp().dot(other);
	if (sign >= 0)
	{
		return angle;
	}
	else
	{
		return -angle;
	}
}

