#include "Player.h"
#include "SpriteBatch.h"
#include "Input.h"
#include <glfw3.h>
#include "Application.h"

Player::Player()
{
}
Player::Player(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice)
{
	m_entityTexture = a_texture;
	position.x = a_pos.x;
	position.y = a_pos.y;
	SetTeam(a_teamChoice);
}
Player::~Player()
{
}

void Player::Sprint(Input* a_input, float deltaTime)
{
	if (a_input->IsKeyDown(GLFW_KEY_SPACE) && canUseSprint == true)
	{
		m_velocity = GetVelocity() * 2.5f;
		canUseSprint = false;
	}
		if (canUseSprint == false)
	{
		sprintResetTimer += deltaTime;
		if (sprintResetTimer >= 2)
		{
			canUseSprint = true;
			sprintResetTimer = 0;
		}
	}
}


void Player::Update(float deltaTime, Input* a_input)
{
	float m_moveSpeed = 50; 
	if (a_input->IsKeyDown(GLFW_KEY_D))
	{
		m_velocity.x += m_moveSpeed;
	}
	if (a_input->IsKeyDown(GLFW_KEY_A))
	{
		m_velocity.x -= m_moveSpeed;
	}
	if (a_input->IsKeyDown(GLFW_KEY_W))
	{
		m_velocity.y -= m_moveSpeed;
	}
	if (a_input->IsKeyDown(GLFW_KEY_S))
	{
		m_velocity.y += m_moveSpeed;
	}

	//Player boundary collision
	if (position.x - m_entityTexture->GetWidth() / 2.0f <= 0.0f)
	{
		position = Vector2(0 + m_entityTexture->GetWidth() / 2.0f, position.y);
	}

	if (position.x + m_entityTexture->GetWidth() / 2.0f >= m_windowBoundaries.x)
	{
		position = Vector2(m_windowBoundaries.x - m_entityTexture->GetWidth() / 2.0f, position.y);
	}

	if (position.y - m_entityTexture->GetWidth() / 2.0f <= 0.0f)
	{
		position = Vector2(position.x, 0 + m_entityTexture->GetWidth() / 2.0f);
	}

	if (position.y + m_entityTexture->GetWidth() / 2.0f >= m_windowBoundaries.y)
	{
		position = Vector2(position.x, m_windowBoundaries.y - m_entityTexture->GetWidth() / 2.0f);
	}
	Sprint( a_input,  deltaTime);
	//move the actual player
	position = position + m_velocity * deltaTime;
	m_velocity = m_velocity * 0.9f;
}



