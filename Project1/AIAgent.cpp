#include "AIAgent.h"
#include "Game1.h"
#include "SoccerDebugDraw.h"

AIAgent::AIAgent()
{
}

AIAgent::AIAgent(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice)
{
	m_entityTexture = a_texture;
	position.x = a_pos.x;
	position.y = a_pos.y;
	SetTeam(a_teamChoice);
}
AIAgent::~AIAgent()
{
}

void AIAgent::Update(float deltaTime)
{
	//std::cout << (int)currState << std::endl;	//Opposition boundary collisions
	if (position.x - m_entityTexture->GetWidth() / 2.0f <= 0.0f)
	{
		position = Vector2(0 + m_entityTexture->GetWidth() / 2.0f, position.y);
	}
	if (position.x + m_entityTexture->GetWidth() / 2.0f >= m_windowBoundaries.x)
	{
		position = Vector2(m_windowBoundaries.x - m_entityTexture->GetWidth() / 2.0f, position.y);
	}
	if (position.y - m_entityTexture->GetWidth() / 2.0f <= 0.0f)
	{
		position = Vector2(position.x, 0 + m_entityTexture->GetWidth() / 2.0f);
	}
	if (position.y + m_entityTexture->GetWidth() / 2.0f >= m_windowBoundaries.y)
	{
		position = Vector2(position.x, m_windowBoundaries.y - m_entityTexture->GetWidth() / 2.0f);
	}
	position = position + GetVelocity() * deltaTime;
	Vector2 zero;
	SetVelocity(GetVelocity() * 0.9f);
}


void AIAgent::SetWindowBoundaries(Vector2 a_boundaries)
{
	m_windowBoundaries = a_boundaries;
}


void AIAgent::Respawn()
{
	Entity::Respawn();


	//extra respawn stuff
	//reset states
}

void AIAgent::BasicSeek(Vector2 desiredPos, AIAgent * a_ai)
{

	// this is a vector to the seek position from the current location of the ai
	Vector2 vecToTargetPosition =  a_ai->position - desiredPos;
	// apply velocity to move AI to position
	a_ai->SetVelocity(a_ai->GetVelocity() + vecToTargetPosition.Normalised()*-40.0f);

}

void AIAgent::AIKick(float deltaTime, AIAgent* a_ai, Ball* a_ball)
{
	avoidSpeed = 0;

	a_ai->SetVelocity(a_ai->GetVelocity() + kickDirection *-0.5f);

}

void AIAgent::AISeek(Ball* a_ball, Goal* a_oppositionGoal, AIAgent* a_ai, float a_windowWidth)
{

	Vector2 offsetFromGoal;

	if (a_oppositionGoal != nullptr)
		offsetFromGoal = a_oppositionGoal->position;

	// the following line works out a vector that points from the goals location to the balls current location
	Vector2 vecFromGoal = (a_ball->position - offsetFromGoal);

	// the seekPos is where we want the ball to aim to get. it determines this by adding the vecFromGoal vector to the balls position plus a certain offset
	SeekPos = a_ball->position + vecFromGoal.Normalised() * m_seekPosOffset;

	// this is a vector to the seek position from the current location of the opposition
	Vector2 vecBetweenSeekOpposition = SeekPos - a_ai->position;

	//this is a vector going from the ball to the opposition
	Vector2 vecBetweenOppositionBall = a_ai->position - a_ball->position;


	//determine the distance between the opposition and the goal they are attacking
	Vector2 vecBetweenOppositionAndGoal = a_ai->position - offsetFromGoal;
	float distanceBetweenOppositionAndGoal = vecBetweenOppositionAndGoal.Magnitude();



	//next two lines check for collision between opposition and seekPos
	Vector2 vecBetweenOppositionSeekPos = a_ai->position - SeekPos;
	//this function checks to see if the seekpos has been reached so that we can change to the seek condition
	if (vecBetweenOppositionSeekPos.Magnitude() <= ((a_ball->GetWidth())) && a_ai-> currState == eAIState::SEEK /*&& distanceBetweenOppositionAndGoal <= (a_windowWidth/2 )*/)
	{
		m_currKickTimer = m_kickTime;
		a_ai->currState = eAIState::KICK;
		kickDirection = vecBetweenOppositionBall;
	}

	if (vecBetweenSeekOpposition.Magnitude() > 40.0f)
	{
		// this sets the oppositions velocity to their current velocity plus a normalised vector pointing at the ball
		// the multiplication of a negative number makes it that the opposition is now actually trying to avoid the ball.
		// this avoidance makes it that the opposition will curve around the ball to get to the seekPos.
		a_ai->SetVelocity(a_ai->GetVelocity() + (a_ball->position - a_ai->position).Normalised() * -avoidSpeed);
		// this sets the oppositions velcotiy to their current velocity plus a vector pointing at the seekPos. this is how we cause movement to the seekPos and if fast enough how we eventually hit the ball
		a_ai->SetVelocity(a_ai->GetVelocity() + (SeekPos - a_ai->position).Normalised() * moveSpeed);
	}
	else
	{
		a_ai->SetVelocity(a_ai->GetVelocity() + vecBetweenOppositionBall.Normalised()*-40.0f);
	}

	//if (a_ball->position.x > (a_windowWidth / 2))
	//{
	//	a_opposition->currState = eAIState::DEFEND;
	//}




}

void AIAgent::AIDefend(Ball* a_ball, Goal*a_playerGoal, AIAgent* a_ai, float a_windowWidth)
{
	////AI movement towards the ball
	avoidSpeed = -20;

	// the following line works out a vector that points from the goals location to the balls current location
	Vector2 vecFromGoal = (a_ball->position - a_playerGoal->position);

	// the seekPos is where we want the ball to aim to get. it determines this by adding the vecFromGoal vector to the balls position plus a certain offset
	SeekPos = a_ball->position + vecFromGoal.Normalised() * -m_seekPosOffset;

	// this is a vector to the seek position from the current location of the opposition
	Vector2 vecBetweenSeekOpposition = SeekPos - a_ai->position;

	//this is a vector going from the ball to the opposition
	Vector2 vecBetweenOppositionBall = a_ai->position - a_ball->position;


	if (vecBetweenSeekOpposition.Magnitude() > 40.0f)
	{
		// this sets the oppositions velcotiy to their current velocity plus a normalised vector pointing at the ball
		// the multiplication of a negative number makes it that the opposition is now actually trying to avoid the ball.
		// this avoidance makes it that the opposition will curve around the ball to get to the seekPos.
		a_ai->SetVelocity(a_ai->GetVelocity() + (a_ball->position - a_ai->position).Normalised() * -avoidSpeed);
		// this sets the oppositions velcotiy to their current velocity plus a vector pointing at the seekPos. this is how we cause movement to the seekPos and if fast enough how we eventually hit the ball
		a_ai->SetVelocity(a_ai->GetVelocity() + (SeekPos - a_ai->position).Normalised() * moveSpeed);
	}
}

void AIAgent::AIHangBackDefence(Goal*a_playerGoal, AIAgent* a_ai, Ball* a_ball)
	{
		

		//this calculates the line from the Goal towards the player
		Vector2 goalToBallLine = a_ball->position - a_playerGoal->position;
		Vector2 defenderPrefferedPosition = a_playerGoal->position + goalToBallLine.Normalised() * 100.0f;
		
		Vector2 velocityTowardsPosition = (a_ai->position - defenderPrefferedPosition).Normalised();
		a_ai->SetVelocity(a_ai->GetVelocity() + (velocityTowardsPosition * (-moveSpeed *2)));
	}

void AIAgent::AIDeflection(Goal* a_oppositionGoal, AIAgent* a_ai, Ball* a_gameBall)
{

	if (a_gameBall->position.y >= (m_windowBoundaries.y / 2))
	{
		if (m_team->GetTeamDirection() == 1)
		{
			Vector2 AttackerPrefferedPosition(a_gameBall->position.x - deflectionDistance, a_oppositionGoal->position.y - 100);
			Vector2 velocityTowardsPosition = (AttackerPrefferedPosition - a_ai->position).Normalised();
			a_ai->SetVelocity(a_ai->GetVelocity() + (velocityTowardsPosition * moveSpeed));
			SoccerDebugDraw::GetInstance()->AddDrawPoint(AttackerPrefferedPosition, 0, 255, 0, 255);
		}

		else
		{
			Vector2 AttackerPrefferedPosition(a_gameBall->position.x + deflectionDistance, a_oppositionGoal->position.y - 100);
			Vector2 velocityTowardsPosition = (AttackerPrefferedPosition - a_ai->position).Normalised();
			a_ai->SetVelocity(a_ai->GetVelocity() + (velocityTowardsPosition * moveSpeed));
			SoccerDebugDraw::GetInstance()->AddDrawPoint(AttackerPrefferedPosition, 0, 255, 0, 255);

		}
	}
	
	if (a_gameBall->position.y < (m_windowBoundaries.y / 2))
	{
		if (m_team->GetTeamDirection() == 1)
		{
			Vector2 AttackerPrefferedPosition(a_gameBall->position.x - deflectionDistance, a_oppositionGoal->position.y + 100);
			Vector2 velocityTowardsPosition = (AttackerPrefferedPosition - a_ai->position).Normalised();
			a_ai->SetVelocity(a_ai->GetVelocity() + (velocityTowardsPosition * moveSpeed));
			SoccerDebugDraw::GetInstance()->AddDrawPoint(AttackerPrefferedPosition, 0, 255, 0, 255);

		}

		else
		{
			Vector2 AttackerPrefferedPosition(a_gameBall->position.x + deflectionDistance, a_oppositionGoal->position.y + 100);
			Vector2 velocityTowardsPosition = (AttackerPrefferedPosition - a_ai->position).Normalised();
			a_ai->SetVelocity(a_ai->GetVelocity() + (velocityTowardsPosition * moveSpeed));
			SoccerDebugDraw::GetInstance()->AddDrawPoint(AttackerPrefferedPosition, 0, 255, 0, 255);

		}
	}


}
void AIAgent::AIBullyDefender(AIAgent* a_thisAI, Character* a_victim)
{
	Vector2 AttackerPrefferedPosition(a_victim->position);
	Vector2 velocityTowardsVictim = (AttackerPrefferedPosition - a_thisAI->position).Normalised();
	a_thisAI->SetVelocity(a_thisAI->GetVelocity() + (velocityTowardsVictim * moveSpeed));
}

void AIAgent::AIFindSpace(AIAgent* a_thisAI, Character* a_defender)
{

}

void AIAgent::AIPatrol(AIAgent * a_thisAI, float a_windowboundariesx, float a_windowBoundariesy)
{
	
	if (a_thisAI->position.y <= a_windowBoundariesy * 0.25f)
	{
		 movedown = true;
		 moveup = false;
	}
	
		if (movedown == true)
			{
				Vector2 nextDestination(a_thisAI->position.x, a_windowBoundariesy * 0.75f);
				Vector2 velocityTowardsDestination = (nextDestination - a_thisAI->position).Normalised();
				a_thisAI->SetVelocity(a_thisAI->GetVelocity() + (velocityTowardsDestination * moveSpeed));
				SoccerDebugDraw::GetInstance()->AddDrawPoint(nextDestination, 0, 255, 0, 255);
			}
		
	
	
	if (a_thisAI->position.y >= a_windowBoundariesy * 0.75f)
	{
		 movedown = false;
		 moveup = true;
	}
		if (moveup == true)
		{
			Vector2 nextDestination(a_thisAI->position.x, a_windowBoundariesy * 0.25f);
			Vector2 velocityTowardsDestination = (nextDestination - a_thisAI->position).Normalised();
			a_thisAI->SetVelocity(a_thisAI->GetVelocity() + (velocityTowardsDestination * moveSpeed));
			SoccerDebugDraw::GetInstance()->AddDrawPoint(nextDestination, 0, 255, 0, 255);
		}
	}

void AIAgent::AIAvoid(AIAgent * a_thisAI, Character * a_murderBot)
{
	Vector2 toMurder = a_murderBot->position - a_thisAI->position;
	float lookAheadTime = toMurder.Magnitude() / (a_thisAI->moveSpeed + a_murderBot->moveSpeed);

	AIFlee(a_murderBot, lookAheadTime);
}

void AIAgent::AIFlee(Character* a_murderBot, float a_lookAheadTime)
{
	
	float distanceFromMurder = Vector2(this->position - a_murderBot->position).Magnitude();
	Vector2 DesiredVelocity = Vector2((this->position - a_murderBot->position).Normalised() * this->moveSpeed) *a_lookAheadTime;
	this->SetVelocity(this->GetVelocity() + (DesiredVelocity * moveSpeed *(0.5 / distanceFromMurder * 5)));

}

Vector2 AIAgent::AIFindMidPoint(Vector2 m_pos1, Vector2 m_pos2)
{
	Vector2 finalPos = ((m_pos1 + m_pos2) *0.5);
	return finalPos;
}





