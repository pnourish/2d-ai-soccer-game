#pragma once
#include "Entity.h"
#include "AIAgent.h"
#include "Player.h"
#include "Ball.h"
#include "Goal.h"
#include "MurderBot.h"
#include "MarkovPlayer.h"


class CollisionManager
{
public:
	CollisionManager();
	~CollisionManager();
//Player and ball Collision
	bool PlayerAndBallCollision(Player* a_player, Ball* a_ball);
//Opposition and ball collision
	bool AIAndBallCollision(AIAgent* a_opposition, Ball* a_ball);
//Ball and PlayerGoal collision
	bool BallAndPlayerGoalCollision(Ball* a_ball, Goal* a_playerGoal, AIAgent* a_opposition, Player* a_player);
//Ball and OppositionGoal collision
	bool BallAndOppositionGoalCollision(Ball* a_ball, Goal* a_playerGoal, AIAgent* a_opposition, Player* a_player);
//Player and opposition collision
	bool PlayerAndAiCollision(Player* a_player, AIAgent* a_Ai);
//opposition and opposition collision
	bool AiAndAiCollision(AIAgent* a_firstAi, AIAgent* a_secondAi);
//ai and murderbot collision
	bool AiAndMurderBotCollision(AIAgent* a_AI, MurderBot* a_murderBot);
//player and murderBot collision
	bool playerAndMurderBotCollision(Player* a_player, MurderBot* a_murderBot);
//ball and murderbot collision
	bool BallAndMurderBotCollision(Ball* a_ball, MurderBot* a_murderBot);
//markov Player and ball collision
	bool MarkovPlayerAndBallCollision(MarkovPlayer * a_markovPlayer, Ball * a_ball);

};

