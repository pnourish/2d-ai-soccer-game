#pragma once
#include "Button.h"
#include "Input.h"
class GameOverScene
{
public:
	GameOverScene();
	~GameOverScene();

	void Startup();//allocate all resources, create ageents, 
				   //create player, create everything
	void Update();
	void Draw(SpriteBatch* m_spritebatch);

	void Shutdown(); //destroy everything

					 //vector of entities
					 //textures (maybe application holds textures

	int windowWidth = 1232;
	int windowHeight = 878;

	int playerScore = 0;
	int opponentScore = 0;

	Button* OpponentScoreButton;
	Button* PlayerScoreButton;
	Button* MainMenu;
	Button* ReMatch;
	Button* Quit;

	Button* WhoWon;
	std::string WhoWonText;
protected:
	SpriteBatch *m_spritebatch;
private:
	Font *m_font;


};