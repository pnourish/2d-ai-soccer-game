#pragma once
#include "Texture.h"
#include "SpriteBatch.h"
#include "Input.h"
#include "Vector2.h"
#include <glfw3.h>
#include "Application.h"
#include "DynamicArrayTemplate.h"
#include "Utility.h"

class Team;
class Entity
{
public:
	Entity();
	Entity(Texture* a_texture, Vector2 a_pos);
	~Entity();
	void Draw(SpriteBatch* a_spriteBatch);
	void SetVelocity(Vector2 a_velocity);
	Vector2 GetVelocity();
	void Drag();
	void SetWindowBoundaries(Vector2 a_boundaries);
	Vector2 m_windowBoundaries;
	Vector2 position;
	virtual void Respawn();
	void SetSpawn(Vector2 spawnPos);
	Vector2 GetSpawn();
	float GetWidth();
	float GetHeight();
	void SetTeam(Team* a_team);
	Team* GetTeam();
	void SetAlive(bool a_lifestate);
	void SetDead();
	bool IsAlive();

	
protected:
	Vector2 m_velocity;
	Vector2 m_spawnPos;
	Texture* m_entityTexture;
	Team* m_team;
	bool m_isAlive = true;


};

