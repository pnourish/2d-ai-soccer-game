#pragma once
#include "Entity.h"
#include "Texture.h"
#include "ISubject.h"

const int EVENT_TOUCHED = 0;
const int EVENT_RELEASED = 1;

class Ball : public Entity, public ISubject
{
public:

	Ball();
	Ball(Texture* a_texture, Vector2 a_pos);
	~Ball();
	void Update( float deltaTime );
	void SetWindowBoundaries(Vector2 a_boundaries);
	Vector2 m_windowBoundaries;
	bool recentlyTouched;
	float recentlyTouchedTimer;
	float recentlyTouchedFullTime = 2;
	void OnKick(float deltaTime, int playerID);
	bool canRelease = true;
	
	
};

