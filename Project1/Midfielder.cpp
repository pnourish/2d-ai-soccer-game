#include "Midfielder.h"
#include "Game1.h"
#include "team.h"

Midfielder::Midfielder(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice) : AIAgent(a_texture, a_pos, a_teamChoice)
{
	m_fieldPosition = eFIELDPOSITION::MIDFIELDER;
	moveSpeed = midfielderSpeed;
}


Midfielder::~Midfielder()
{
}




void Midfielder::Update(float deltaTime)
{

	Vector2 vecBetweenOppositionBall = position - m_team->GetBall()->position;
	Vector2 vecBetweenOppositionSeekPos = position - SeekPos;
		for (int i = 0; i < m_fleePositions.Size(); i++)
		{
			Vector2 murderVec(this->position - m_fleePositions[i]->position);
			float distanceToMurder = murderVec.Magnitude();
			if (distanceToMurder <= m_avoidDistance)
			{
				AIAgent::AIAvoid(this, m_fleePositions[i]);
			}
		}

	//switch
	switch (currState)

	{
	case eAIState::KICK:
		AIKick(deltaTime, this, m_team->GetBall());

		//do the transitions from here
		m_currKickTimer -= deltaTime;
		if (m_currKickTimer <= 0)
		{
			currState = eAIState::SEEK;
		}

		if (vecBetweenOppositionBall.Magnitude() <= ((m_team->GetBall()->GetWidth() / 2) + (GetWidth() / 2)))
		{
			if (m_currKickTimer > 0.1f)
				m_currKickTimer = 0.1f;
		}

		 
		break;



	case eAIState::SEEK:
		
		AISeek(m_team->GetBall(), m_team->GetGoal(), this, m_windowBoundaries.x);




		if (vecBetweenOppositionSeekPos.Magnitude() <= ((m_team->GetBall()->GetWidth())) && currState == eAIState::SEEK)
		{
			m_currKickTimer = m_kickTime;
			currState = eAIState::KICK;
			kickDirection = vecBetweenOppositionBall;
		}

	}


	AIAgent::Update(deltaTime);
}
