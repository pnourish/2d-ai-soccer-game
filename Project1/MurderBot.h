#pragma once
#include "AIAgent.h"
class MurderBot : public AIAgent
{
public:
	MurderBot(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice);
	~MurderBot();


	void Update(float deltaTime);
};

