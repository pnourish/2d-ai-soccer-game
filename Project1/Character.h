#pragma once
#include "Entity.h"

class Team;
class Character : public Entity
{
public:
	enum class eFIELDPOSITION
	{
		ATTACKER,
		MIDFIELDER,
		DEFENDER,
		MURDERBOT, 
		MARKOVOPPOSITION

	};

	eFIELDPOSITION m_fieldPosition;

	Character();
	~Character();


	void AddFleePosition( Character* a_murderBot);
	void RemoveFleePosition(Character* a_murderBot);
	float moveSpeed;

protected:
	DynamicArrayTemplate<Character*> m_fleePositions;
	float m_avoidDistance = 200;


};

