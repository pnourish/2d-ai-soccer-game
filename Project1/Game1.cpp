#include "Game1.h"
#include "SpriteBatch.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "glfw3.h"
#include "Application.h"
#include "Vector2.h"
#include <iostream>
#include <amp.h>
#include "amp.h"
#include <ctime>
#include "AIAgent.h"
#include "Attacker.h"
#include "Defender.h"
#include "Midfielder.h"
#include "MurderBot.h"	
#include "DynamicArrayTemplate.h"
#include "team.h"

#include "SoccerDebugDraw.h"


Game1::Game1(unsigned int windowWidth, unsigned int windowHeight, bool fullscreen, const char *title) : Application(windowWidth, windowHeight, fullscreen, title)
{
	// Creating the sprite batch
	m_spritebatch = SpriteBatch::Factory::Create(this, SpriteBatch::GL3);

	SoccerDebugDraw::GetInstance();
	SwitchScene(eScene::MENU);
	return;
}

Game1::~Game1()
{
	// DELETE EVERYTHING!!!
	SpriteBatch::Factory::Destroy(m_spritebatch);
	//keepingsOffScene.Shutdown();
	SoccerDebugDraw::DestroySingleton();
	
}


void Game1::Update(float deltaTime)
{
	mousePos.x = Input::GetSingleton()->GetMouseX();
	mousePos.y = Input::GetSingleton()->GetMouseY();
	switch (currScene)
	{
	case eScene::MENU:
		menuScene.Update();
		if (menuScene.standardGame->checkPos(mousePos) == true && Input::GetSingleton()->WasMouseButtonPressed(GLFW_MOUSE_BUTTON_LEFT))
		{
			SwitchScene(eScene::STANDARDGAME);
			return;
		}
		if (menuScene.KeepingsOff->checkPos(mousePos) == true && Input::GetSingleton()->WasMouseButtonPressed(GLFW_MOUSE_BUTTON_LEFT))
		{
			SwitchScene(eScene::KEEPINGSOFF);
			return;
		}
		if (menuScene.Markov->checkPos(mousePos) == true && Input::GetSingleton()->WasMouseButtonPressed(GLFW_MOUSE_BUTTON_LEFT))
		{
			SwitchScene(eScene::MARKOV);
			return;
		}

		if (menuScene.Quit->checkPos(mousePos) == true && Input::GetSingleton()->WasMouseButtonPressed(GLFW_MOUSE_BUTTON_LEFT))
		{
			menuScene.Shutdown();
			Quit();
			return;
		}
		break;
	case eScene::STANDARDGAME:
		gameScene.Update(deltaTime);
		if (gameScene.hasGameEnded == true)
		{
			gameScene.hasGameEnded = false;
			SwitchScene(eScene::GAMEOVER);
			return;
		}
		break;
	case eScene::KEEPINGSOFF:
		keepingsOffScene.Update(deltaTime);
		if (keepingsOffScene.hasGameEnded == true)
		{
			keepingsOffScene.hasGameEnded = false;
			SwitchScene(eScene::GAMEOVER);
			return;
		}
		break;
	case eScene::MARKOV:
		markovScene.Update(deltaTime);
		if (markovScene.hasGameEnded == true)
		{
			markovScene.hasGameEnded = false;
			SwitchScene(eScene::GAMEOVER);
			return;
		}
		break;
	case eScene::GAMEOVER:
		//gameOverScene.Shutdown();
		if (gameOverScene.MainMenu->checkPos(mousePos) == true && Input::GetSingleton()->WasMouseButtonPressed(GLFW_MOUSE_BUTTON_LEFT))
		{
			SwitchScene(eScene::MENU);
			return;
		}
		if (gameOverScene.ReMatch->checkPos(mousePos) == true && Input::GetSingleton()->WasMouseButtonPressed(GLFW_MOUSE_BUTTON_LEFT))
		{
			SwitchScene(prevScene);
			return;
		}
		if (gameOverScene.Quit->checkPos(mousePos) == true && Input::GetSingleton()->WasMouseButtonPressed(GLFW_MOUSE_BUTTON_LEFT))
		{
			gameOverScene.Shutdown();
			Quit();
			return;
		}
		break;
	}
}
void Game1::Draw()
{
	// clear the back buffer
	ClearScreen();
	m_spritebatch->Begin();

	switch (currScene)
	{
	case eScene::MENU:
		menuScene.Draw(m_spritebatch);
		break;
	case eScene::STANDARDGAME:
		gameScene.Draw(m_spritebatch);
		break;
	case eScene::KEEPINGSOFF:
		keepingsOffScene.Draw(m_spritebatch);
		break;
	case eScene::MARKOV:
		markovScene.Draw(m_spritebatch);
		break;
	case eScene::GAMEOVER:
		gameOverScene.Draw(m_spritebatch);
		break;
	}
	m_spritebatch->End();


}

void Game1::SwitchScene(eScene nextScene)

{
	switch (currScene)
	{
	case eScene::MENU:
		menuScene.Shutdown();
		break;
	case eScene::STANDARDGAME:
		gameScene.Shutdown();
		break;
	case eScene::KEEPINGSOFF:
		keepingsOffScene.Shutdown();
		break;
	case eScene::MARKOV:
		markovScene.Shutdown();
		break;
	case eScene::GAMEOVER:
		gameOverScene.Shutdown();
		break;
	}
	prevScene = currScene;
	currScene = nextScene;

	switch (currScene)
	{
	case eScene::MENU:
		menuScene.Startup();
		break;
	case eScene::STANDARDGAME:
		gameScene.Startup();
		break;
	case eScene::KEEPINGSOFF:
		keepingsOffScene.Startup();
		break;
	case eScene::MARKOV:
		markovScene.Startup();
		break;
	case eScene::GAMEOVER:
		gameOverScene.Startup();
		break;
	}
}



