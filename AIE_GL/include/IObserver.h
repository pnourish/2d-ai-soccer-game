#pragma once


class IObserver
{
public:
	virtual void OnNotify(int EventID, void* data) = 0;
};