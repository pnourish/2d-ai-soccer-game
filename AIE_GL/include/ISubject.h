#pragma once
#include "IObserver.h"
#include <vector>
class ISubject
{
public:
	void AddObserver(IObserver* observer) { m_observers.push_back(observer); }
	void Notify(int EventID, void* data)
	{
		for (auto& observer : m_observers)
		{
			observer->OnNotify(EventID, data);
		}
	}
private:
	std::vector<IObserver*> m_observers;
};