#include "Application.h"



Application::Application()
{
	gameIsRunning = true;
	
}


Application::~Application()
{
}

void Application::execute()
{

	while (gameIsRunning)
	{

		Update();
		Draw();
	}
}

void Application::Startup()
{

	switch (currScene)
	{
	case eScene::MENU:
		menuScene.Startup();
		break;
	case eScene::GAME:
		gameScene.Startup();
		break;
	}
	
}

void Application::Update()
{
	switch (currScene)
	{
	case eScene::MENU:
		menuScene.Update();
		break;
	case eScene::GAME:
		gameScene.Update();
		break;
	}

}

void Application::Draw()
{
	switch (currScene)
	{
	case eScene::MENU:
		menuScene.Draw();
		break;
	case eScene::GAME:
		gameScene.Draw();
		break;
	}
}

void Application::Shutdown()
{
	switch (currScene)
	{
	case eScene::MENU:
		menuScene.Shutdown();
		break;
	case eScene::GAME:
		gameScene.Shutdown();
		break;
	}
}

void Application::SwitchScene(eScene nextScene)
{
	switch (currScene)
	{
	case eScene::MENU:
		menuScene.Shutdown();
		break;
	case eScene::GAME:
		gameScene.Shutdown();
		break;
	}

	currScene = nextScene;

	switch (currScene)
	{
	case eScene::MENU:
		menuScene.Startup();
		break;
	case eScene::GAME:
		gameScene.Startup();
		break;
	}
}
