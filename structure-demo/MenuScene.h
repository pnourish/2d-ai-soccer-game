#pragma once
class MenuScene
{
public:
	MenuScene();
	~MenuScene();

	void Startup(); //allocate all resources, create
	//agents, create player, create everything
	void Update();
	void Draw();

	void Shutdown(); //destroy everything

	//vector of entities
	//textures (maybe Application holds textures)
};

