#pragma once

#include "GameScene.h"
#include "MenuScene.h"
class Application
{
public:
	enum class eScene
	{
		MENU,
		GAME,
		CREDITS
	};

	Application();
	~Application();

	void execute();

	void Startup();
	void Update();
	void Draw();
	void Shutdown();

	eScene currScene;
	void SwitchScene(eScene nextScene);

	GameScene gameScene;
	MenuScene menuScene;

	bool gameIsRunning = true;
};

